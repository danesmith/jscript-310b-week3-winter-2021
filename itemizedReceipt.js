// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

// Alex Palmmieri helped explain this code to me
// He deserves credit for helping me understand

function logReceipt(...items) {
  let total = 0;
  let itemCount = 0;

  items.forEach((item) => {
    total = total + item.price; //defining total 
    itemCount++; //adding items together
    console.log(`${item.descr} - ${item.price}`);
  });
  console.log(`Total (${itemCount} items) - ${total.toFixed(2)}`);
};

logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);


// with taxes and I am happy to have figured this out with research

function logReceipt(...items) {
  let total = 0;
  let itemCount = 0;

  items.forEach((item) => {
    total = total + item.price; //defining total 
    itemCount++; //adding items together
    taxes = total * 0.08
    console.log(`${item.descr} - ${item.price}`);
  });
  console.log(`Total (${itemCount} items) - ${total.toFixed(2)}`);
  console.log(taxes)
  let subTotal = total + taxes;
  console.log(subTotal); 
};


// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97
