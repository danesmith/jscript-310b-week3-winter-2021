
const RESULT_VALUES = {
  w: 3,
  d: 1,
  l: 0
}

/**
 * Takes a single result string and (one of 'w', 'l', or 'd') 
 * and returns the point value
 * 
 * @param {string} result 
 * @returns {number} point value
 */
const getPointsFromResult = function getPointsFromResult(result) {
  return RESULT_VALUES[result];
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won


function getTotalPoints(str) {
  
  // turn str into an array of letters.

  // initalize a total count variable at zero
    // on each 

  //step two iterate over with forEach and getPointsFromResults

   // str = 'wwwdl'
  
  const letterArray = str.split('')

  let totalPoints = 0
  letterArray.forEach((letter) => {
    let totalResults = RESULT_VALUES[letter];
    totalPoints += totalResults;

    
    
    
    
  })
  return totalPoints;
  



};


// Check getTotalPoints
console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"

function orderTeams(...args) {

  args.forEach((team) => {
    let totalPoints = getTotalPoints(team.results)
    console.log(`${team.name}, ${totalPoints}`) 
    console.log(team)
  })




}






// Check orderTeams
orderTeams(
  { name: 'Sounders', results: 'wwdl' },
  { name: 'Galaxy', results: 'wlld' }
);
// should log the following to the console:
// Sounders: 7
// Galaxy: 4